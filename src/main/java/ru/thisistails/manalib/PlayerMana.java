package ru.thisistails.manalib;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * Represents player mana status.
 */
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@ToString
public class PlayerMana {

    public static PlayerMana of(Player player) {
        return ManaData.getManaData().getPlayerManaByUuid(player.getUniqueId());
    }

    public static PlayerMana of(UUID uuid) {
        return ManaData.getManaData().getPlayerManaByUuid(uuid);
    }

    /**
     * UUID of player which has this stats.
     * @see Player
     */
    private @Getter UUID uuid;

    /**
     * Current player mana.
     */
    private @Getter double mana;

    /**
     * Max player mana.
     */
    private @Getter double maxMana;

    /**
     * Represents player mana regeneration.
     */
    private @Getter double manaRegen;
    
    /**
     * Sets mana to player.
     * @param value     Mana to add.
     */
    public void setMana(double value) {
        mana = MathUtils.clampDouble(value, 0, maxMana);
        ManaData.getManaData().updateCurrentMana(uuid, mana);
    }

    public void addMana(double amount) {
        mana += MathUtils.clampDouble(amount, 0, maxMana);
        ManaData.getManaData().updateCurrentMana(uuid, mana);
    }

    /**
     * Sets max mana for player.
     * @param value    Max mana
     * @return          true if max mana has been changed successfully
     */
    public boolean setMaxMana(double value) {
        FileConfiguration config = Bukkit.getPluginManager().getPlugin("ManaLib").getConfig();
        double configMaxMana = config.getInt("maxAllowedMana");
        
        if (configMaxMana != 0)
            if (value > configMaxMana) return false;

        maxMana = value;
        ManaData.getManaData().updateMaxMana(uuid, value);

        return true;
    }

    /**
     * Sets mana regen for player.
     * @param value     How much player will regenerate in 20 ticks.
     * @return          true if mana regen has been changed successfully
     */
    public boolean setManaRegeneration(double value) {
        FileConfiguration config = Bukkit.getPluginManager().getPlugin("ManaLib").getConfig();
        double configMaxManaRegen = config.getInt("maxAllowedManaRegen");
        
        if (configMaxManaRegen != 0)
            if (value > configMaxManaRegen) return false;

        manaRegen = value;
        ManaData.getManaData().updateManaRegen(uuid, value);

        return true;
    }
}
