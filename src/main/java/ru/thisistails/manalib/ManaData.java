package ru.thisistails.manalib;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import lombok.Getter;

public class ManaData {

    private @Getter Connection con;
    private @Getter Statement cursor;

    private static ManaData instance = null;

    public static ManaData getManaData() {
        if (instance == null) instance = new ManaData();

        return instance;
    }

    private ManaData() {}

    private final String jdbcString = "jdbc:sqlite:" + Bukkit.getPluginManager().getPlugin("ManaLib").getDataFolder().getAbsolutePath() + "/data.sqlite";

    protected void setupDatabase() {
        connect();
        FileConfiguration config = Bukkit.getPluginManager().getPlugin("ManaLib").getConfig();

        int playerMana = config.getInt("defaultPlayerMana");
        int playerManaRegen = config.getInt("defaultPlayerManaRegen");

        try {
            cursor.execute(
                String.format("create table if not exists ManaData(uuid TEXT NOT NULL, mana REAL DEFAULT %d, manaregen REAL DEFAULT %d, maxmana REAL DEFAULT %d, UNIQUE(uuid))",
                playerMana, playerManaRegen, playerMana));
        } catch (SQLException e) {
            e.printStackTrace();
            Bukkit.getLogger().severe("Failed to create SQLite database. Restart server with '/rl confirm' or try to fix this error by yourself.");
        }
    }

    public void connect() {
        try {
            con = DriverManager.getConnection(jdbcString);
            con.setAutoCommit(true);
            cursor = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            cursor.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Add new record to data. Will fail silently if record already exists.
     * @param player    Player to add.
     */
    public void addNewPlayer(Player player) {
        try {
            cursor.execute("INSERT OR IGNORE INTO ManaData(uuid) VALUES('" + player.getUniqueId().toString() + "')");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public PlayerMana getPlayerManaByUuid(UUID uuid) {
        ResultSet set;
        PlayerMana mana = null;

        try {
            set = cursor.executeQuery("SELECT * FROM ManaData WHERE uuid = '" + uuid.toString() + "'");
            
            while (set.next()) {
                double curMana, maxMana, manaRegen;
                curMana = set.getDouble(2);
                maxMana = set.getDouble(4);
                manaRegen = set.getDouble(3);

                mana = new PlayerMana(UUID.fromString(set.getString(1)), curMana, maxMana, manaRegen);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return mana;
    }

    public boolean updateCurrentMana(UUID uuid, double mana) {
        try {
            cursor.execute(String.format("UPDATE ManaData SET mana = " + mana + " WHERE uuid = '%s'", uuid));
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateManaRegen(UUID uuid, double regenAmount) {
        try {
            cursor.execute(String.format("UPDATE ManaData SET manaregen = " + regenAmount + " WHERE uuid = '%s'", uuid));
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateMaxMana(UUID uuid, double newMaxMana) {
        try {
            cursor.execute(String.format("UPDATE ManaData SET maxmana = " + newMaxMana + " WHERE uuid = '%s'", uuid));
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

}
