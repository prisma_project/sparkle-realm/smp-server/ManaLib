package ru.thisistails.manalib;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import ru.thisistails.manalib.Commands.GetManaInfo;
import ru.thisistails.manalib.Commands.SetManaCommand;
import ru.thisistails.manalib.Listeners.JoinEvent;

public class Main extends JavaPlugin {

    @Override
    public @NotNull FileConfiguration getConfig() {
        return require("config.yml");
    }

    @Override
    public void onDisable() {
        ManaData.getManaData().close();
    }

    @Override
    public void onEnable() {
        getConfig();
        ManaData.getManaData().setupDatabase();
        PluginManager manager = getServer().getPluginManager();
        manager.registerEvents(new JoinEvent(), this);

        getCommand("getPlayerMana").setExecutor(new GetManaInfo());
        
        getCommand("setPlayerMana").setExecutor(new SetManaCommand());
        getCommand("setPlayerMana").setTabCompleter(new SetManaCommand());

        for (Player player : getServer().getOnlinePlayers()) {
            ManaData.getManaData().addNewPlayer(player);
        }

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

            @Override
            public void run() {
                for (Player player : getServer().getOnlinePlayers()) {
                    PlayerMana mana = PlayerMana.of(player);
                    mana.addMana(mana.getManaRegen());
                }
            }
            
        }, 20, 20);
    }

    private FileConfiguration require(String filePath) {
        final String pluginName = "ManaLib";

        File file = new File(Bukkit.getPluginManager().getPlugin(pluginName).getDataFolder() + "/" + filePath);

        if (!file.exists()) {
            try {
                //locFile.mkdir();
                Bukkit.getPluginManager().getPlugin(pluginName).saveResource(filePath, false);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return null;
            }
        }

        return YamlConfiguration.loadConfiguration(file);

    }

    
}
