package ru.thisistails.manalib.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import net.md_5.bungee.api.ChatColor;
import ru.thisistails.manalib.PlayerMana;

public class GetManaInfo implements CommandExecutor {

    private final String infoString = "\"%s:" +
                "&8UUID&f: %s" +
                "&8Current mana&f: &d" +
                "&8Maximum mana&f: %d" +
                "&8Mana regenration&f: %d";

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {

        if (args.length == 0) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("You are not a player!");
                return true;
            }

            Player player = (Player) sender;
            PlayerMana mana = PlayerMana.of(player);

            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', 
            String.format(infoString, mana.getUuid(), mana.getMana(), mana.getMaxMana(), mana.getManaRegen())));
            return true;
        }

        Player player = Bukkit.getPlayer(args[0]);
        if (player == null) {
            sender.sendMessage("Player not found.");
            return true;
        }

        PlayerMana mana = PlayerMana.of(player);
        sender.sendMessage(mana.toString());

        return true;
    }
    
}
