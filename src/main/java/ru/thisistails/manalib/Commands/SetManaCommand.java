package ru.thisistails.manalib.Commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.thisistails.manalib.PlayerMana;

public class SetManaCommand implements CommandExecutor, TabCompleter {

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {

        if (args.length < 3) return false;

        Player player = Bukkit.getServer().getPlayer(args[0]);
        if (player == null) {
            sender.sendMessage("Player not found.");
            return true;
        }

        PlayerMana playerMana = PlayerMana.of(player);

        final String option = args[1].toLowerCase();
        float value = 0;

        try {
            value = Float.parseFloat(args[2]);
        } catch (NumberFormatException e) {
            sender.sendMessage("Wrong argument.");
            return false;
        }

        switch (option) {
            case "mana":
                playerMana.setMana(value);
                sender.sendMessage("You set current mana to " + args[2] + " for player " + player.getName());
                break;
            
            case "maxmana":
                playerMana.setMaxMana(value);
                sender.sendMessage("You set maximum mana to " + args[2] + " for player " + player.getName());
                break;
            
            case "manaregen":
                playerMana.setManaRegeneration(value);
                sender.sendMessage("You set mana regeneration to " + args[2] + " for player " + player.getName());
                break;
        
            default:
                sender.sendMessage("Wrong option.");
                return false;
        }

        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (args.length == 2) {
            List<String> options = new ArrayList<>();
            options.add("mana");
            options.add("maxMana");
            options.add("manaRegen");

            return options;
        }

        return null;
    }
    
}
