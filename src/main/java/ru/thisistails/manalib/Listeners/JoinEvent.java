package ru.thisistails.manalib.Listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import ru.thisistails.manalib.ManaData;

public class JoinEvent implements Listener {
    
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        ManaData.getManaData().addNewPlayer(player);
    }

}
